package com.shukur.birthday_bot.bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Component
public class BirthdayBot extends TelegramLongPollingBot {

    private static final Logger LOG = LoggerFactory.getLogger(BirthdayBot.class);
    private static final String START = "/start";
    private static final String MESSAGE4U = "/message4u";
    private static final String WISH = "/wish";
    private static final String HELP = "/help";

    public BirthdayBot(@Value("${bot.token}") String botToken) {

        super(botToken);
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (!update.hasMessage() || !update.getMessage().hasText()) {
            return;
        }

        var message = update.getMessage().getText();
        var chatId = update.getMessage().getChatId();

        switch (message) {
            case START -> {
                String userName = update.getMessage().getChat().getUserName();
                startCommand(chatId, userName);
            }
            case MESSAGE4U -> message4uCommand(chatId);
            case WISH -> wishCommand(chatId);
            case HELP -> helpCommand(chatId);
            default -> unknownCommand(chatId);
        }
    }

    private void startCommand(Long chatId, String userName) {
        var text = """
                Добро пожаловать в бот, Nuri!
                
                Посредством этого бота я смогу донести до тебя "bütün ürək sözlərimi" :D.
                
                Для этого воспользуйся командами по-очередно:
                1. /message4u
                2. /wish
                3. /help
                
                Или же можешь написать любую команду, но только в самом начале => до выполнения шагов 1-3. :)
                
                Удачи в дальнейших действиях!
               
                """;
        var formattedText = String.format(text, userName);
        sendMessage(chatId, formattedText);
    }

    private void message4uCommand(Long chatId) {
        var text = """
                Дорогой друг,
                
                С днём рождения! В этот особенный день хочу напомнить тебе о самом важном сокровище, которым мы обладаем – нашем здоровье и здоровье наших близких.
                
                Пусть каждый твой день будет наполнен радостью и энергией. Желаю тебе крепости духа и тела, чтобы каждое твоё утро начиналось с улыбки и чувства благодарности за очередной прекрасный день.
                
                Заботься о себе, цени моменты, проведённые с семьёй и друзьями, ведь именно здоровые и счастливые отношения делают нашу жизнь ярче и значимее. Помни, что забота о собственном здоровье – это не только личная ответственность, но и лучший подарок для тех, кто нас окружает.
                
                С любовью и наилучшими пожеланиями,
                
                Мурад!
                
                """;
        var formattedText = String.format(text);
        sendMessage(chatId, formattedText);
    }

    private void wishCommand(Long chatId) {
        var text = """
                Əslində bundan əvvəlki arzular
                
                chatGpt tərəfindən hazırlanıb, lakin düşünürəm ki, botun yazılışına sərf olunan vaxtı nəzərə alıb anlayacaqsan ki, urəkdən səni təbrik etmək istəyirdim və bundan əlavə olaraq qeyri adi bir təbrik metodu istədim. :D
                
                Bu arada 1-2 söz özümdəndə deyim, onsuzda mənim arzularımı çox güman ki bilirsən...
                
                Arzu edirəm ki, bütün arzulariva və məqsədlərivə çatasan, xoşbəxt və dərdsiz həyat sürəsən, həmişə yaxıların sənin ətrafında olsun, xoşbəxtlik, var-dövlət, vəzifə, və ən əsası uzun ğmür və can salığı sənə və yaxınlariva arzu edirəm!!!
                
                Hə, az qala yadımdan çıxmışdır, senin borcuvu sənə qaytarmağımı sənə və ğzümə arzu edirəm! :D
               
                """;
        var formattedText = String.format(text);
        sendMessage(chatId, formattedText);
    }

    private void helpCommand(Long chatId) {
        var text = """
                Bu Botu yazmaq üçün 2 saat vaxt sərf etdim, 
                
                Lakin düşünürəm ki, təcrübəm daha çox olsa idi bunu 10-15 dəqiqəyə yaza bilərdim :D
                
                Bu bölməni sirf bu məlumatı sənə çatdırmaq üçün yaratmışam. :/
                
                P.s bilirəm çox mənasız bölmədir... Ona görədə "Öpürəm brat" , расти большой :*
                
                
                """;
        var formattedText = String.format(text);
        sendMessage(chatId, formattedText);
    }

    private void unknownCommand(Long chatId) {
        var text = "Zarafat edirəm adə, Düzgün seçim elə debil :D";
        sendMessage(chatId, text);
    }

    @Override
    public String getBotUsername() {
        return "Sh_Programming_Lab_Project_2_Bot";
    }

    private void sendMessage(Long chatId, String text) {
        var chatIdStr = String.valueOf(chatId);
        var sendMessage = new SendMessage(chatIdStr,text);
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            LOG.error("Something wrong with mail sender", e);
        }
    }
}
