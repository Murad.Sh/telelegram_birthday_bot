package com.shukur.birthday_bot.configuration;

import com.shukur.birthday_bot.bot.BirthdayBot;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

@Configuration
public class BirthdayBotConfiguration {
    @Bean
    public TelegramBotsApi telegramBotsApi(BirthdayBot birthdayBot) throws TelegramApiException {
        var api = new TelegramBotsApi(DefaultBotSession.class);
        api.registerBot(birthdayBot);
        return api;
    }
}
